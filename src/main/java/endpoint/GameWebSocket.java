package endpoint;

import java.io.IOException;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketListener;

public class GameWebSocket implements WebSocketListener {

    private Session session;

    public void onWebSocketBinary(byte[] bytes, int i, int i1) {

    }

    public void onWebSocketText(String s) {
        System.out.println("New message arrived from " + session.getLocalAddress() + ": \n" + s);
        try {
            session.getRemote().sendString("MESSAGE RECEIVED!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onWebSocketClose(int i, String s) {
        System.out.println("Session disconnected from websocket");
        session.close();
    }

    public void onWebSocketConnect(Session session) {
        session = session;
        System.out.println("New session connected on websocket: " + session.getLocalAddress());
        try {
            session.getRemote().sendString("CONNECTED");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onWebSocketError(Throwable throwable) {
        System.out.println("An error occurred");
    }
}